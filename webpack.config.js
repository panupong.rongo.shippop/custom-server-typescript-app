const path = require('path')
const nodeExternals = require('webpack-node-externals')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const WebpackHookPlugin = require('webpack-hook-plugin').default

const dev = process.env.NODE_ENV !== 'production'
module.exports = {
  mode: dev ? 'development' : 'production',
  entry: {
    server: './src/index.ts',
  },
  output: {
    path: path.join(__dirname, './build/'),
    publicPath: '/',
    filename: 'index.js',
  },
  target: 'node',
  module: {
    rules: [
      {
        test: /\.(js|jsx|tsx|ts)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.tsx', '.ts'],
  },
  node: {
    __dirname: false,
    __filename: false,
  },
  externals: [nodeExternals()],
  plugins: [
    ...(dev
      ? [
          new WebpackHookPlugin({
            onBuildEnd: ['nodemon'],
          }),
        ]
      : [new CleanWebpackPlugin()]),
  ],
}
