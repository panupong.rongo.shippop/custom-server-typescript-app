module.exports = {
  serverRuntimeConfig: {
    app: {
      env: process.env.ENV,
      port: process.env.NODE_PORT,
    },
  },
  publicRuntimeConfig: {
    env: process.env.ENV,
  },

  distDir: 'build/.next',
}
