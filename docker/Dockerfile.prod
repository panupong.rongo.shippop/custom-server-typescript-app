########## install_modules_stage ##########
FROM node:12-alpine as install_modules_stage

# set working directory
WORKDIR /app/
# install production dependencies
COPY package.json yarn.lock /app/
RUN yarn install --frozen-lockfile --production

########## build_stage ##########
FROM install_modules_stage as build_stage

# install all dependencies
RUN yarn install --frozen-lockfile

# copy all files
COPY .babelrc webpack.config.js next.config.js jsconfig.json .env /app/
COPY src/ /app/src/
COPY public/ /app/public/

# build the production app
RUN yarn build


########## final_stage ##########
FROM node:12-alpine as final_stage

# set working directory
WORKDIR /app/

# copy package.json, next.config.js, and .env file
COPY --from=build_stage /app/package.json /app/next.config.js /app/.env /app/

# copy production deps
COPY --from=install_modules_stage /app/node_modules/ /app/node_modules/

# copy public files
COPY --from=build_stage /app/public/ /app/public/

# copy production app
COPY --from=build_stage /app/build/ /app/build/

EXPOSE 3000
# default command
CMD ["yarn", "start"]
