import express, { Application, Response } from 'express'
import nextJS from 'next'
import helmet from 'helmet'
import cors, { CorsOptions } from 'cors'

export const initServer = async (port: number): Promise<Application> => {
  const development = process.env.NODE_ENV !== 'production'
  const nextApp = nextJS({ dev: development })
  const handle = nextApp.getRequestHandler()

  await Promise.all([nextApp.prepare()])
  const app: Application = express()
  // middlewares
  app.disable('etag')
  app.use(helmet({ contentSecurityPolicy: false }))

  // ------------ cors -----------
  const options: CorsOptions = {
    origin: '*',
    credentials: true,
  }

  app.use(cors(options))
  // --------------------------
  app.get('/api/status', (_, resp: Response) => resp.send({ message: 'OK' }))

  app.get('*', (request, resp) => handle(request, resp))

  app.listen(port, () => {
    console.info(`> Server is running on Port: ${port}`)
  })

  return app
}
