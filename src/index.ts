import { initServer } from './server/app'

// eslint-disable-next-line no-void
const port = process.env.NODE_PORT
  ? Number.parseInt(process.env.NODE_PORT, 10)
  : 3000

console.info(`port`, port)

// eslint-disable-next-line no-void
void initServer(port)
