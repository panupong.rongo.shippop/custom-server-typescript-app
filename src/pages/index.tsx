import React, { ReactElement } from 'react'
import Button from '@material-ui/core/Button'
// interface Properties {}

function index(): ReactElement {
  // {}: Properties
  const x = 1

  console.info('x', x)

  return (
    <div>
      Index-2
      <Button variant="contained" color="primary" type="button">
        xxx
      </Button>
    </div>
  )
}

export default index
